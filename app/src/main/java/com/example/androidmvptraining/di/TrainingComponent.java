package com.example.androidmvptraining.di;

import com.example.androidmvptraining.service.ApiService;

import dagger.Component;

@TrainingScope
@Component(modules = {ApiServiceModule.class})
public interface TrainingComponent {
    ApiService getService();
}
