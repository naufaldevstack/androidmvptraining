package com.example.androidmvptraining.di;

import com.example.androidmvptraining.service.ApiService;
import com.google.gson.Gson;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = NetworkModule.class)
public class ApiServiceModule {
    private static final String BASE_URL = "https://jsonplaceholder.typicode.com/";

    @Provides
    @TrainingScope
    public ApiService mService(@Named("retrofit") Retrofit retrofit){
        return retrofit.create(ApiService.class);
    }

    @Provides
    @TrainingScope
    public Gson gson(){
        return new Gson();
    }

    @Provides
    @TrainingScope
    @Named("baseUrl")
    public String baseUrl() {
        return BASE_URL;
    }

    @Provides
    @TrainingScope
    @Named("retrofit")
    public Retrofit retrofit(OkHttpClient client, Gson gson, @Named("baseUrl") String baseUrl){
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }
}