package com.example.androidmvptraining.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class SampleModel {
    @SerializedName("userId") private int userId;
    @SerializedName("id") private int id;
    @SerializedName("title") private String title;
    @SerializedName("completed") private boolean completed;
}
