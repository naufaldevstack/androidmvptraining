package com.example.androidmvptraining.features.sample;

import com.example.androidmvptraining.di.TrainingComponent;

import dagger.Component;

@SampleScope
@Component (modules = SampleModule.class, dependencies = TrainingComponent.class)
public interface SampleComponent {
    SampleContract.Presenter getPresenter();
}
