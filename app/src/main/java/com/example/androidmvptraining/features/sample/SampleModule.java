package com.example.androidmvptraining.features.sample;

import dagger.Module;
import dagger.Provides;

@Module
public class SampleModule {
    private final SampleContract.View mView;

    public SampleModule(SampleContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @SampleScope
    SampleContract.View sampleView() { return mView; }

    @Provides
    @SampleScope
    SampleContract.Presenter samplePresenter(SamplePresenter presenter){
        return presenter;
    }
}
