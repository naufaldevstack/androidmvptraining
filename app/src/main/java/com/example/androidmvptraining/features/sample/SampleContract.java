package com.example.androidmvptraining.features.sample;

import com.example.androidmvptraining.model.SampleModel;

public interface SampleContract {
    interface View {
        void onGetSuccess(SampleModel model);
        void onGetFailed(String error);
    }

    interface Presenter {
        void getToDo();
    }
}
