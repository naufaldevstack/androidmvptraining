package com.example.androidmvptraining.features.sample;

import com.example.androidmvptraining.model.SampleModel;
import com.example.androidmvptraining.service.ApiService;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SamplePresenter implements SampleContract.Presenter{
    @Inject
    SampleContract.View mView;
    @Inject
    ApiService mService;

    @Inject
    public SamplePresenter() {
        //Empty Constructor Required
    }

    @Override
    public void getToDo() {
        mService.getToDo().enqueue(new Callback<SampleModel>() {
            @Override
            public void onResponse(Call<SampleModel> call,
                                   Response<SampleModel> response) {
                if(response.isSuccessful()){
                    mView.onGetSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<SampleModel> call, Throwable t) {
                mView.onGetFailed("Failed");
            }
        });
    }
}
