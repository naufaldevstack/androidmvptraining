package com.example.androidmvptraining.features.sample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.androidmvptraining.CustomApplication;
import com.example.androidmvptraining.R;
import com.example.androidmvptraining.model.SampleModel;

public class SampleActivity extends AppCompatActivity implements SampleContract.View {
    TextView textView;
    private SampleContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.text_view);

        //Get Presenter
        SampleComponent sampleComponent = DaggerSampleComponent.builder()
                .trainingComponent(CustomApplication.get(this).getComponent())
                .sampleModule(new SampleModule(this))
                .build();

        mPresenter = sampleComponent.getPresenter();

        //Loading
        mPresenter.getToDo();
    }

    @Override
    public void onGetSuccess(SampleModel model) {
        textView.setText(model.getTitle());
    }

    @Override
    public void onGetFailed(String error) {
        textView.setText(error);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
