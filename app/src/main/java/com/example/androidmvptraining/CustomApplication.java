package com.example.androidmvptraining;

import android.app.Activity;
import android.app.Application;
import android.os.StrictMode;

import com.example.androidmvptraining.di.ContextModule;
import com.example.androidmvptraining.di.DaggerTrainingComponent;
import com.example.androidmvptraining.di.TrainingComponent;

import timber.log.Timber;

public class CustomApplication extends Application {
    private TrainingComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Timber.plant(new Timber.DebugTree());

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        mComponent = DaggerTrainingComponent.builder()
                .contextModule(new ContextModule(this)).build();
    }

    public static CustomApplication get(Activity activity) {
        return (CustomApplication) activity.getApplication();
    }

    public TrainingComponent getComponent() {
        return mComponent;
    }
}
