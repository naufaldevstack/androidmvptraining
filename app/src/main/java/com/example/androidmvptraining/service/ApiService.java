package com.example.androidmvptraining.service;

import com.example.androidmvptraining.model.SampleModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @GET("todos/1")
    Call<SampleModel> getToDo();
}
